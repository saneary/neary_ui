﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public Animator weaponbutton;
    public Animator armorbutton;
    public Animator weaponpanel;
    public Animator armorpanel;
    public Animator bowpanel;
    public Animator swordpanel;
    public Animator axepanel;
    // Start is called before the first frame update
    public void OpenMenu()
    {
        armorbutton.SetBool("MenuOpen", true);
        weaponbutton.SetBool("MenuOpen", true);
    }
    public void OpenWeapons()
    {
        weaponpanel.SetBool("MenuOpen", true);
    }
    public void OpenArmor()
    {
        armorpanel.SetBool("MenuOpen", true);
    }
    public void CloseMenu()
    {
        armorbutton.SetBool("MenuOpen", false);
        weaponbutton.SetBool("MenuOpen", false);
        weaponpanel.SetBool("MenuOpen", false);
        armorpanel.SetBool("MenuOpen", false);
        bowpanel.SetBool("MenuOpen", false);
        swordpanel.SetBool("MenuOpen", false);
        axepanel.SetBool("MenuOpen", false);
    }
    public void OpenBow()
    {
        bowpanel.SetBool("MenuOpen", true);
    }
    public void OpenSword()
    {
        swordpanel.SetBool("MenuOpen", true);
    }
    public void OpenAxe()
    {
        axepanel.SetBool("MenuOpen", true);
    }
}
